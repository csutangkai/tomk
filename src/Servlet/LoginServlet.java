package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// 获得前台session
//		HttpSession session = req.getSession();
		String name = req.getParameter("username");
		String pwd = req.getParameter("pwd");
		// 创建数据库的连接
		boolean flag = UserDao.getSelect(name, pwd);
		if (flag == true) {
			//重定向
			res.sendRedirect("success.jsp");
		} else
			//转发
			//res.sendRedirect("fail.jsp");
        req.getRequestDispatcher("/fail.jsp").forward(req, res);  
	}
}
