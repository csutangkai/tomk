package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;

public class RegistServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		doGet(req,res);
	}
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException{
		String name=req.getParameter("name");
		String pwd=req.getParameter("pwd");
		//创建数据库
		boolean flag=UserDao.getInsert(name,pwd);
		if(flag==true){
			res.sendRedirect("login.jsp");
		}else
			res.sendRedirect("fail.jsp");
	}
}
